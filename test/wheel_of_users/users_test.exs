defmodule WheelOfUsers.UsersTest do
  use WheelOfUsers.DataCase, async: true

  alias WheelOfUsers.{Repo, User, Utils}
  alias WheelOfUsers.Users, as: Subject

  test "#insert_all/1 inserts all users in the list" do
    users = [
      %{points: 0, inserted_at: timestamp(), updated_at: timestamp()},
      %{points: 0, inserted_at: timestamp(), updated_at: timestamp()}
    ]

    Subject.insert_all(users)

    assert Repo.aggregate(User, :count) == 2
  end

  test "#insert_all/1 doesn't insert users when argument is not a list" do
    users = %{
      users: [
        %{points: 0, inserted_at: timestamp(), updated_at: timestamp()},
        %{points: 0, inserted_at: timestamp(), updated_at: timestamp()}
      ]
    }

    {0, nil} = Subject.insert_all(users)

    assert Repo.aggregate(User, :count) == 0
  end

  test "#update_points_with_random_value/1 updates all users with a random value between 0 and ceil parameter" do
    users = [
      %{points: 0, inserted_at: timestamp(), updated_at: timestamp()},
      %{points: 0, inserted_at: timestamp(), updated_at: timestamp()}
    ]

    Subject.insert_all(users)

    max_value = 75

    Subject.update_points_with_random_value(max_value)

    [user1 | [user2 | _]] = Repo.all(User)

    assert user1.points in 0..max_value
    assert user2.points in 0..max_value
  end

  test "#update_points_with_random_value/1 updates all users with 0 if ceil parameter is not an integer greater than 0" do
    users = [
      %{points: 0, inserted_at: timestamp(), updated_at: timestamp()},
      %{points: 0, inserted_at: timestamp(), updated_at: timestamp()}
    ]

    Subject.insert_all(users)

    max_value = -1

    {0, nil} = Subject.update_points_with_random_value(max_value)

    [user1 | [user2 | _]] = Repo.all(User)

    assert user1.points == 0
    assert user2.points == 0
  end

  test "#get_by_points_greater_than_score gets all users with points greater than score parameter" do
    users = [
      %{points: 25, inserted_at: timestamp(), updated_at: timestamp()},
      %{points: 7, inserted_at: timestamp(), updated_at: timestamp()},
      %{points: 41, inserted_at: timestamp(), updated_at: timestamp()}
    ]

    Subject.insert_all(users)

    score = 7

    [user1 | [user2 | _]] = Subject.get_by_points_greater_than_score(score)

    assert user1.points == 25
    assert user2.points == 41
  end

  test "#get_by_points_greater_than_score returns an empty list when there is no users with points greater than score" do
    users = [
      %{points: 3, inserted_at: timestamp(), updated_at: timestamp()},
      %{points: 7, inserted_at: timestamp(), updated_at: timestamp()},
      %{points: 10, inserted_at: timestamp(), updated_at: timestamp()}
    ]

    Subject.insert_all(users)

    score = 10

    assert [] = Subject.get_by_points_greater_than_score(score)
  end

  test "#get_by_points_greater_than_score returns an empty list when parameter is invalid" do
    users = [
      %{points: 3, inserted_at: timestamp(), updated_at: timestamp()},
      %{points: 7, inserted_at: timestamp(), updated_at: timestamp()}
    ]

    Subject.insert_all(users)

    score = "5"

    assert [] = Subject.get_by_points_greater_than_score(score)
  end

  defp timestamp do
    Utils.generate_current_timestamp()
  end
end
