defmodule WheelOfUsers.UsersManagerTest do
  use ExUnit.Case, async: false
  import Mox

  alias WheelOfUsers.UsersMock
  alias WheelOfUsers.{User, Utils}
  alias WheelOfUsers.UsersManager, as: Subject

  @name __MODULE__
  @users []
  @timestamp Utils.generate_current_timestamp()
  @initial_state %{users: @users, timestamp: @timestamp}

  setup :set_mox_from_context
  setup :verify_on_exit!

  setup do
    {:ok, process_pid} = Subject.start_link(@initial_state, name: @name)
    {:ok, %{users: process_pid}}
  end

  test "#users/1 returns users stored in state" do
    response = Subject.get_users(@name)
    users = response[:users]

    assert users == @users
  end

  test "#users/1 returns the previous timestamp" do
    response = Subject.get_users(@name)
    timestamp = response[:timestamp]

    assert timestamp == @timestamp
  end

  test "#get_all/0 calls users repository to fetch users", %{
    users: process_pid
  } do
    parent = self()

    UsersMock
    |> expect(:get_by_points_greater_than_score, fn _value ->
      send(parent, {process_pid, :get_by_points_greater_than_score})
      :ok
    end)

    Process.send(process_pid, :get_all, [])

    assert_receive {^process_pid, :get_by_points_greater_than_score}
  end

  test "#get_all/0 updates state with max of 2 users randomly picked", %{
    users: process_pid
  } do
    users = [
      %User{id: 1, points: 75},
      %User{id: 2, points: 62},
      %User{id: 3, points: 82}
    ]

    parent = self()

    UsersMock
    |> expect(:get_by_points_greater_than_score, fn _value ->
      send(parent, {process_pid, :get_by_points_greater_than_score})
      users
    end)

    state = :sys.get_state(process_pid)

    assert length(state.users) == 0

    Process.send(process_pid, :get_all, [])

    new_state = :sys.get_state(process_pid)

    assert length(new_state.users) <= number_of_users_to_select()
  end

  test "#update_all/0 updates points attribute of all users with a random number", %{
    users: process_pid
  } do
    parent = self()

    UsersMock
    |> expect(:update_points_with_random_value, fn _value ->
      send(parent, {process_pid, :update_points_with_random_value})
      :ok
    end)

    Process.send(process_pid, :update_all, [])

    assert_receive {^process_pid, :update_points_with_random_value}
  end

  defp number_of_users_to_select do
    Application.get_env(:wheel_of_users, WheelOfUsers.UsersManager)[:number_of_users_to_select]
  end
end
