defmodule WheelOfUsers.UserTest do
  use WheelOfUsers.DataCase, async: true

  alias WheelOfUsers.User

  describe "users" do
    @user_attrs %{points: 21}

    test "points is not required" do
      changeset = User.changeset(%User{}, Map.delete(@user_attrs, :points))
      assert changeset.valid?
    end

    test "points should be an integer" do
      changeset = User.changeset(%User{}, %{@user_attrs | points: "invalid_value"})
      refute changeset.valid?
    end

    test "points should be an integer in the range between 0 and 100" do
      changeset = User.changeset(%User{}, %{@user_attrs | points: 101})
      refute changeset.valid?
    end
  end
end
