defmodule WheelOfUsersWeb.UsersControllerTest do
  use WheelOfUsersWeb.ConnCase
  import Mox

  setup :verify_on_exit!

  describe "#index" do
    @users_manager_response %{users: [%{id: 1, points: 123_456}], timestamp: DateTime.utc_now()}

    test "returns a payload with selected users and a timestamp", %{conn: conn} do
      WheelOfUsers.UsersManagerAPIMock
      |> expect(:get_users, fn -> @users_manager_response end)

      conn = get(conn, "/")

      body = json_response(conn, 200)

      assert Map.has_key?(body, "users")
      assert Map.has_key?(body, "timestamp")

      users = body["users"]
      assert length(users) <= number_of_users_to_select()
    end
  end

  defp number_of_users_to_select do
    Application.get_env(:wheel_of_users, :number_of_users_to_select)
  end
end
