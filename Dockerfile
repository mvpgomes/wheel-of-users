FROM elixir:latest

ENV APP_HOME /app
ARG MIX_ENV
ENV MIX_ENV ${MIX_ENV:-dev}

WORKDIR $APP_HOME

RUN mix local.hex --force && \
    mix local.rebar --force && \
    mix hex.info

COPY mix.* $APP_HOME/

RUN mix do deps.get, deps.compile

COPY . $APP_HOME/

RUN mix compile