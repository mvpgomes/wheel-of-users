# Wheel Of Users

![](https://spinthewheel.app/assets/images/preview/pointowheel-239Z.png)

A web application that queries random users.

## Requirements
This application is fully dockerized. The only requirement is to have `docker` and `docker-compose` installed.

## Running the application
In order to launch the application run the following command:

```bash
$ docker-compose up app
```

The application will start and be exposed at `localhost:4000`. In order to query by users please run the following command:

```bash
$ curl localhost:4000/
```

## Running the tests
In order to run the application tests run the following command:

```bash
$ docker-compose up tests
```

## Architecture
The application was implemented according to the specifications of this task. The most relevant modules of the application are:

- `WheelOfUsersController:` The controller responsible for processing `GET /` request. This module only calls `UsersManager` and returns its response.
- `WheelOfUsers.Users:`: Module that implements the logic around Ecto, i.e, the queries that are executed by the application.
- `WheelOfUsers.Seeder.Users`: Module that implements the logic around the application initial seeding. 
- `WheelOfUsers.UsersManager`: GenServer that implements the application's main behaviour. This GenServer exposes a public interface to query the random users and schedule random updates every minute. In the next section we will provide some implementation details about this GenServer.

### Users Manager
As mentioned in the above section, `UsersManager` is responsible for implementing the application main logic, namely querying and updating the existing users. The logic of updating all users is quite simple and rely on `Repo.update_all/2` and `fragment()` to update user's points with a new random number.

The logic of querying the users that have a given number of points turned to be more complex due to the performance of the query to search users. Since the query is executed over the whole table, even with the index on `points` attribute, the overall performance is not very good (worst case scenario around ~8s), which was causing the GenServer to timeout in `handle_call/2`. After considering some factors, the logic of fetching users was refactored to pre-compute the results of the query in background and persist it in the process state. As result, when a request is received to query by users we return the pre=computed result instead of querying the database. 

## Improvements
This application still need some improvements, such as:

- Improve overall performance of Postgres queries by using another strategy to query by users.
- Decouple `Users` implementation of Postgres. For instance, we rely in `fragment()` to generate random points for a user. In the future, if we decided to switch to another database (no-SQL), this query will not work.
- Refactor some parts of the project structure and code organization, such as defining the behaviours in dedicated files, extracting some functionality outside the GenServer, etc.
- Improve overall error handling.
- Add instrumentation to all relevant modules.
- Refactor Dockerfile to use Mix releases.
