alias WheelOfUsers.{Repo, User}

# Cleanup Users database before seeding initial data
Repo.delete_all(User)

# Seed initial Users data
WheelOfUsers.Seeder.Users.run()
