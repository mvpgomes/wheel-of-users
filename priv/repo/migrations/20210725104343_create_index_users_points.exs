defmodule WheelOfUsers.Repo.Migrations.CreateIndexUsersPoints do
  use Ecto.Migration

  def change do
    create index(:users, [:points])
  end
end
