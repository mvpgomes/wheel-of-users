use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :wheel_of_users, WheelOfUsers.Repo,
  username: "postgres",
  password: "postgres",
  database: "wheel_of_users_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: "postgres",
  pool: Ecto.Adapters.SQL.Sandbox

config :wheel_of_users,
  users_manager_api: WheelOfUsers.UsersManagerAPIMock,
  users_repository: WheelOfUsers.UsersMock

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :wheel_of_users, WheelOfUsersWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

config :wheel_of_users,
  random_numbers_range: 1..100
