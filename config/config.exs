# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :wheel_of_users,
  ecto_repos: [WheelOfUsers.Repo]

config :wheel_of_users,
  users_manager_api: WheelOfUsers.UsersManagerAPI,
  users_repository: WheelOfUsers.Users

config :wheel_of_users, WheelOfUsers.UsersManager,
  number_of_users_to_select: 2,
  random_number_ceil: 100

config :wheel_of_users, WheelOfUsers.Seeder,
  user_points: 0,
  users_count: 1_000_000

config :wheel_of_users, WheelOfUsers.Repo,
  username: System.get_env("POSTGRES_USER"),
  password: System.get_env("POSTGRES_PASSWORD"),
  database: System.get_env("POSTGRES_DATABASE"),
  hostname: System.get_env("POSTGRES_HOST"),
  port: System.get_env("POSTGRES_PORT"),
  show_sensitive_data_on_connection_error: true,
  pool_size: String.to_integer(System.get_env("POSTGRES_POOLSIZE", "10")),
  migration_timestamps: [type: :utc_datetime]

# Configures the endpoint
config :wheel_of_users, WheelOfUsersWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ChePz//hnvsUnfiNwsdZqSp8ZGy5Ax1cDatGr4k1yUVOo3wU82KuGd5DBkkEg84l",
  render_errors: [view: WheelOfUsersWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: WheelOfUsers.PubSub,
  live_view: [signing_salt: "4N4mP3uO"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
