defmodule WheelOfUsers.User do
  use Ecto.Schema
  import Ecto.Changeset

  @points_range 0..100

  schema "users" do
    field :points, :integer, default: 0

    timestamps(type: :utc_datetime_usec)
  end

  @doc false
  def changeset(users, attrs) do
    users
    |> cast(attrs, [:points])
    |> validate_inclusion(:points, @points_range)
  end
end
