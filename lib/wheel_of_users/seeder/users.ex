defmodule WheelOfUsers.Seeder.Users do
  @moduledoc """
  Seeders.Users module is responsible for performing the application's initial seeding.

  The number of users to be seeded in configurable through the environment variable

  `Application.get_env(:wheel_of_users, WheelOfUsers.Seeder)[:users_count]` 

  The default points for each users if configurable through the environment variable

  `Application.get_env(:wheel_of_users, WheelOfUsers.Seeder)[:user_points]`

  The seed mechanism is wrapped around an transaction in order to abort the overall
  seeding if an operation fails.
  """
  alias WheelOfUsers.{Repo, Utils}

  @users_chunk_size 20_000
  @user_points Application.get_env(:wheel_of_users, WheelOfUsers.Seeder)[:user_points]
  @users_count Application.get_env(:wheel_of_users, WheelOfUsers.Seeder)[:users_count]

  @users_repository Application.get_env(:wheel_of_users, :users_repository)

  @spec run() :: {:ok, any()} | {:error, any()}
  def run do
    Repo.transaction(fn ->
      Stream.iterate(user(), & &1)
      |> Enum.take(@users_count)
      |> Stream.chunk_every(@users_chunk_size)
      |> Task.async_stream(fn users ->
        @users_repository.insert_all(users)
      end)
      |> Stream.run()
    end)
  end

  defp user do
    datetime = Utils.generate_current_timestamp()

    %{points: @user_points, inserted_at: datetime, updated_at: datetime}
  end
end
