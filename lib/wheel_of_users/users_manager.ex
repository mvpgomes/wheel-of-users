defmodule WheelOfUsers.UsersManager do
  @moduledoc """
  `UsersManager` module implements the main application logic.

  Exposes an interface to get a maximum of 2 users where `points` is higher than `max_number`.

  And schedules an update to all users points every 60s.
  """

  use GenServer

  alias WheelOfUsers.Utils

  @users_repository Application.get_env(:wheel_of_users, :users_repository)

  @doc """
  Pre-compute the users given the internal state and returns the previous pre-computed users
  and the last response timestamp.

  ## Examples

  iex> WheelOfUsers.UsersManager.get_users()
  %{users: [%{id: 1, points: 45}], timestamp: "2021-07-28 22:28:00.720322Z"}

  """
  def get_users(name \\ __MODULE__) do
    GenServer.call(name, :users)
  end

  def start_link(args, opts \\ []) do
    name = Keyword.get(opts, :name, __MODULE__)

    GenServer.start_link(__MODULE__, initial_state(args), name: name)
  end

  def init(state) do
    schedule_update()

    {:ok, state}
  end

  def handle_call(:users, _from, state) do
    schedule_get_users()

    {:reply, build_reply(state), update_state(:timestamp, state)}
  end

  def handle_info(:get_all, %{max_number: max_number} = state) do
    users =
      @users_repository.get_by_points_greater_than_score(max_number)
      |> Enum.take_random(number_of_users_to_select())
      |> Enum.map(&Map.from_struct(&1))

    {:noreply, update_state(:users, state, users)}
  end

  def handle_info(:update_all, state) do
    @users_repository.update_points_with_random_value(random_number_ceil())

    schedule_update()

    {:noreply, update_state(:max_number, state)}
  end

  defp initial_state(args) do
    %{
      max_number: Map.get(args, :max_number, Utils.generate_random_number(random_number_ceil())),
      timestamp: Map.get(args, :timestamp, nil),
      users: []
    }
  end

  defp schedule_update do
    Process.send_after(self(), :update_all, 60 * 1000)
  end

  defp schedule_get_users do
    Process.send_after(self(), :get_all, 1)
  end

  defp build_reply(state) do
    %{users: state.users, timestamp: state.timestamp}
  end

  defp update_state(:timestamp, state) do
    %{state | timestamp: Utils.generate_current_timestamp()}
  end

  defp update_state(:max_number, state) do
    %{state | max_number: Utils.generate_random_number(random_number_ceil())}
  end

  defp update_state(:users, state, users) do
    %{state | users: users}
  end

  defp number_of_users_to_select do
    Application.get_env(:wheel_of_users, WheelOfUsers.UsersManager)[:number_of_users_to_select]
  end

  defp random_number_ceil do
    Application.get_env(:wheel_of_users, WheelOfUsers.UsersManager)[:random_number_ceil]
  end
end
