defmodule WheelOfUsers.Utils do
  @moduledoc """
  `Utils` module contains common utils functions used across the project.
  """

  @doc """
  Generates a random number between 0 and `ceil`. It returns `0` when a invalid parameter is provided.

  ## Examples

  iex> WheelOfUsers.Utils.generate_random_number(45)
  19

  iex> WheelOfUsers.Utils.generate_random_number(:invalid_parameter)
  0
  """
  @spec generate_random_number(any()) :: integer()
  def generate_random_number(ceil) when is_integer(ceil),
    do: Enum.random(0..ceil)

  def generate_random_number(_ceil), do: 0

  @doc """
  Generates the current timestamp.

  ## Examples

  iex> WheelOfUsers.Utils.generate_current_timestamp()
  ~U[2021-07-28 22:27:25.780081Z]
  """
  @spec generate_current_timestamp() :: DateTime.t()
  def generate_current_timestamp, do: DateTime.utc_now()
end
