defmodule WheelOfUsers.UsersManagerAPI do
  @moduledoc """
  `UsersManagerAPI` module is wrapper around `UserManager` that was made exclusively to
  make the application more testable when using `Mox`.
  """

  alias WheelOfUsers.UsersManager

  @callback get_users() :: map()

  @doc """
  Calls `UsersManager.get_users().

  Returns a list of users and a timestamp.

  ## Examples

  iex> WheelOfUsers.UsersManagerAPI.get_users()
  %{users: [%{id: 1, points: 45}], timestamp: "2021-07-28 22:28:00.720322Z"}

  """
  def get_users do
    UsersManager.get_users()
  end
end
