defmodule WheelOfUsers.Users do
  @moduledoc """
  `Users` module implements the logic around `Repo`.
  """

  import Ecto.Query
  require Logger

  alias WheelOfUsers.{Repo, User}

  @callback insert_all(list()) :: {integer(), nil | [term()]}
  @callback update_points_with_random_value(integer()) :: {integer(), nil | [term()]}
  @callback get_by_points_greater_than_score(integer()) :: list()

  @doc """
  Inserts the list of users. Returns a tuple with the number of created users.

  When an invalid parameter is received it doesn't creates users.

  ## Examples

  iex> WheelOfUsers.Users.insert_all([%{points: 45, inserted_at: "2021-07-28 22:28:00.720322Z", updated_at: "2021-07-28 22:28:00.720322Z"}])
  {1, _}

  iex> WheelOfUsers.Users.insert_all(%{})
  {0, nil}

  """
  def insert_all(users) when is_list(users) do
    Repo.insert_all(User, users)
  end

  def insert_all(users) do
    Logger.error(message: "Invalid parameter when creating users", users: IO.inspect(users))

    {0, nil}
  end

  @doc """
  Updates all users with a random value between 0 and `ceil`. Returns a tuple with the number of updated users.

  When an invalid parameter is received it doesn't update users.

  ## Examples

  iex> WheelOfUsers.Users.update_points_with_random_value(50)
  {1_000_000, _}

  iex> WheelOfUsers.Users.update_points_with_random_value(:invalid_value)
  {0, nil}

  """
  def update_points_with_random_value(ceil) when is_integer(ceil) and ceil > 0 do
    update(User, set: [points: fragment("floor(random()*?)", ^ceil)])
    |> Repo.update_all([])
  end

  def update_points_with_random_value(ceil) do
    Logger.error(message: "Invalid parameter when updating users", ceil: IO.inspect(ceil))

    {0, nil}
  end

  @doc """
  Returns all users with points higher than `score`.

  When an invalid parameter is received it doesn't creates users.

  ## Examples

  iex> WheelOfUsers.Users.update_points_with_random_value(50)
  [%User{id: 1, points: 78}, ..., %User{id: 589, points: 51}]

  iex> WheelOfUsers.Users.update_points_with_random_value(:invalid_value)
  {0, nil}

  """
  def get_by_points_greater_than_score(score) when is_integer(score) do
    from(u in User, where: u.points > ^score)
    |> Repo.all()
  end

  def get_by_points_greater_than_score(score) do
    Logger.error(
      message: "Invalid parameter when getting users by score",
      score: IO.inspect(score)
    )

    []
  end
end
