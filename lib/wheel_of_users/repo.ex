defmodule WheelOfUsers.Repo do
  use Ecto.Repo,
    otp_app: :wheel_of_users,
    adapter: Ecto.Adapters.Postgres
end
