defmodule WheelOfUsersWeb.UsersView do
  use WheelOfUsersWeb, :view
  alias WheelOfUsersWeb.UsersView

  def render("index.json", %{users: users, timestamp: timestamp}) do
    %{
      users: render_many(users, UsersView, "user.json"),
      timestamp: timestamp
    }
  end

  def render("user.json", %{users: user}) do
    %{id: user.id, points: user.points}
  end
end
