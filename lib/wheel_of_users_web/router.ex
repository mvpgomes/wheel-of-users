defmodule WheelOfUsersWeb.Router do
  use WheelOfUsersWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", WheelOfUsersWeb do
    pipe_through :api

    get "/", UsersController, :index
  end
end
