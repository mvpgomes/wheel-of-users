defmodule WheelOfUsersWeb.UsersController do
  use WheelOfUsersWeb, :controller

  @users_manager_api Application.get_env(:wheel_of_users, :users_manager_api)

  def index(conn, _params) do
    %{users: users, timestamp: timestamp} = @users_manager_api.get_users()

    render(conn, "index.json", users: users, timestamp: timestamp)
  end
end
